import Entity from "./Entity"
import Bullet from "./Bullet";
import {Howl} from "howler"
import laser from "url:../sound/laser1.wav"
class Player extends Entity{

    accelerating;
    dir;
    lastShoot;
    turning;
    laserSound;
    lives;


    constructor() {
        super();

        this.lives = 3
        this.laserSound = new Howl({
            src: [laser]
        });

        this.accelerating = false
        this.dir = 0
        this.lastShoot = 0
        this.turning = 0
    }

    accelerate(t){
        if (this.accelerating){
            const accel = 0.0003

            const ax = Math.cos(this.dir) * accel
            const ay = Math.sin(this.dir) * accel

            this.vx += ax*t
            this.vy += ay*t
            if(this.vx > 0.5){
                this.vx = 0.5
            }
            if(this.vy > 0.5){
                this.vy = 0.5
            }
            if(this.vx < -0.5){
                this.vx = -0.5
            }
            if(this.vy < -0.5){
                this.vy = -0.5
            }
        } else {
            const accel = 0.0001
            if(this.vy>accel*t){
                this.vy -= accel*t
            }else if(this.vy< -accel*t) {
                this.vy += accel * t
            } else {
                this.vy = 0
            }
            if(this.vx>accel*t){
                this.vx -= accel*t
            } else if(this.vx< -accel*t) {
                this.vx += accel * t
            } else {
                this.vx = 0
            }

        }
    }

    turn(t){
        if(this.turning){
            this.dir+=this.turning * 0.1
        }
    }

    shoot(){
        if(this.lastShoot < Date.now() - 500){
            this.laserSound.play()
            this.lastShoot = Date.now()
            const bullet = new Bullet()
            bullet.x = this.x
            bullet.y = this.y
            bullet.vx = Math.cos(this.dir)
            bullet.vy = Math.sin(this.dir)
        }
    }
}

export default Player