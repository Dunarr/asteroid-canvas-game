const width = window.innerWidth
const height = window.innerHeight

class Entity{
    x;
    y;
    vx;
    vy;

    constructor() {
        this.x = 0;
        this.y = 0;
        this.vx = 0;
        this.vy = 0;
    }

    move(t){
        this.x=(this.x+width+this.vx*t)%width
        this.y=(this.y+height+this.vy*t)%height
    }
}

export default Entity