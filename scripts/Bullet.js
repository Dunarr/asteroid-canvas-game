import Entity from "./Entity";

class Bullet extends Entity{
    static bullets = [];
    createdAt;

    constructor() {
        super();
        this.createdAt = Date.now()
        Bullet.bullets.push(this)
    }
}

export default Bullet