import Player from "./Player"
import Bullet from "./Bullet";
import Meteor from "./Meteor";

import space from "url:../images/space.jpg"
import ship from "url:../images/ship.png"
import bullet from "url:../images/bullet.png"
import meteor from "url:../images/meteor.png"

const spaceImg = new Image()
spaceImg.src = space

const shipImg = new Image()
shipImg.src = ship

const bulletImg = new Image()
bulletImg.src = bullet
const meteorImg = new Image()
meteorImg.src = meteor


let points = 0

import explosion from "url:../sound/explosion.wav"
import music from "url:../sound/music.wav"

import {Howl} from "howler"

const explosionSound = new Howl({
    src: [explosion],
    volume: 0.3,
})

const musicSound = new Howl({
    src: [music],
    loop: true
})
musicSound.play()


const width = window.innerWidth
const height = window.innerHeight
const canvas = document.getElementById("app")

let playing = true

canvas.height = height
canvas.width = width

const ctx = canvas.getContext("2d")
ctx.font = "bold 40px monospace";


const player = new Player()

player.x = width / 2
player.y = height / 2


const imgRatio = 1600 / 1200
const screenRatio = width/height
let imgWidth, imgHeight

if(imgRatio > screenRatio) {
    imgWidth = height * imgRatio
    imgHeight = height
} else {
    imgWidth = width
    imgHeight = width / imgRatio

}



let lastTime

function loop() {
    if (playing) {
        requestAnimationFrame(time => {

            const deltaTime = time - lastTime
            lastTime = time

            ctx.clearRect(0, 0, width, height)


            ctx.drawImage(spaceImg, 0, 0, imgWidth, imgHeight)

            player.turn(deltaTime)
            player.accelerate(deltaTime)
            player.move(deltaTime)

            ctx.fillStyle ="yellow"
            ctx.fillText(player.lives, 50, 50)
            ctx.fillText(points, 50, 100)
            ctx.fillStyle ="black"

            Bullet.bullets = Bullet.bullets.filter(b => b.createdAt > Date.now() - 900)

            Bullet.bullets.forEach(b => {
                b.move(deltaTime)
                drawBullet(b.x, b.y)
            })
            Meteor.meteors = Meteor.meteors.filter(m => m.size > 0)

            Meteor.meteors.forEach(m => {
                m.move(deltaTime)

                Bullet.bullets.forEach(b => {
                    if (isColliding(m.x, m.y, b.x, b.y, m.size * 25, 10)) {
                        points++
                        m.size--

                        m.vx = Math.random() * 0.1 - 0.05
                        m.vy = Math.random() * 0.1 - 0.05

                        const m2 = new Meteor()
                        m2.x = m.x
                        m2.y = m.y
                        m2.vx = Math.random() * 0.1 - 0.05
                        m2.vy = Math.random() * 0.1 - 0.05
                        m2.size = m.size

                        b.createdAt = 0
                    }
                })


                if (isColliding(m.x, m.y, player.x, player.y, m.size * 25, 35)) {
                    explosionSound.play()
                    player.lives--
                    player.x = width / 2
                    player.y = height / 2

                    if (player.lives <= 0) {
                        gameOver()
                    }
                }
                drawMeteor(m.x, m.y, m.size)
            })
            drawPlayer(player.x, player.y, player.dir)
            loop()
        })

    }
}


function handleKeyDown(evt) {
    console.log('hello');
    if (evt.code === "ArrowUp") {
        player.accelerating = true
    }
    if (evt.code === "ArrowRight") {
        player.turning = 1
    }
    if (evt.code === "ArrowLeft") {
        player.turning = -1
    }
    if (evt.code === "Space") {
        player.shoot()
    }
}

window.removeEventListener("keydown", handleKeyDown)

window.addEventListener("keydown", handleKeyDown)


window.addEventListener("keyup", evt => {
    if (evt.code === "ArrowUp") {
        player.accelerating = false
    }


    if (evt.code === "ArrowRight") {
        player.turning = 0
    }
    if (evt.code === "ArrowLeft") {
        player.turning = 0
    }
})


requestAnimationFrame(time => {
    lastTime = time
    loop()
})

function drawPlayer(x, y, dir) {
    ctx.save()
    ctx.translate(x, y);
    ctx.rotate(dir)
    ctx.drawImage(shipImg, -50, -36)
    ctx.restore()



}

setInterval(() => {
    const meteor = new Meteor();
    meteor.x = Math.random() * width
    meteor.y = Math.random() * height
    meteor.vx = Math.random() * 0.1 - 0.05
    meteor.vy = Math.random() * 0.1 - 0.05
    meteor.size = Math.floor(Math.random() * 3) + 1
}, 5000)

function drawBullet(x, y) {
    ctx.drawImage(bulletImg, x-2, y-2, 20, 20)
}

function drawMeteor(x, y, size) {
   ctx.drawImage(meteorImg, x - (size*50/2), y - (size*50/2),size*50, size*50)
}

module.exports = {
    width, height
}

function isColliding(x1, y1, x2, y2, r1, r2) {
    const distX = x2 - x1
    const distY = y2 - y1
    const dist = (distX ** 2 + distY ** 2) ** 0.5
    return dist < r1 + r2
}

function gameOver() {
    playing = false
    musicSound.stop()
    ctx.fillRect(0, 0, width, height)
    ctx.fillStyle = "white"
    ctx.fillText("GAME OVER", width / 2 - 150, height / 2)
}